# SmartLight Simulation  
  
**Projekt zaliczeniowy z przedmiotu Studio Projektowe. **  
Projekt składa się z aplikacji służącej do symulacji wykorzystania systemu inteligentnego oświetlenia ulic oraz odpowiedniej dokumentacji.  
  
**Frontend:** framework Angular + bootstrap + css3  
**Metodyka:** programowanie ekstremalne (programowanie w parach)
  
------------------------------------------------------  
  
## Uruchomienie aplikacji w trybie deweloperskim  
1. Aktualizujemy biblioteki zewnętrzne z wykorzystaniem narzędzia npm:  
	npm update  
2. Uruchamiamy serwer deweloperski:  
	ng serve  
3. Aplikacja została uruchomiona na domyslnym porcie (4200). Adres:  
	http://localhost:4200/  

## Budowanie aplikacji w trybie produkcyjnym 
Aplikację budujemy poleceniem:  
	ng build --prod  

## Dokumentacja  
Dokumentacja udostępniona jest w postaci statycznej strony www i znajduje się w katalogu: /documentation.  
Dokumentacja generowana jest automatycznie z wykorzystaniem narzędzia compodoc. Proces generowania można wywołać poleceniem:  
	npm run compodoc 
	
### Zrzuty ekranu aplikacji
#### Widok tworzenia mapy  
![zrzut 1](src/assets/ssc/z1.jpg)  
#### Przykład manualnego (brzydkiego:)) rozmieszczenia lamp na mapie  
![zrzut 2](src/assets/ssc/z2.jpg) 
#### Przykładowy widok ekranu symulacji   
![zrzut 3](src/assets/ssc/z3.jpg)  
#### Przykładowy widok ekranu symulacji  
![zrzut 4](src/assets/ssc/z4.jpg)
